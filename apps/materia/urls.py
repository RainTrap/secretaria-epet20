from django.urls import path, include
from rest_framework import routers
from .views import (
MateriaViewSet, MateriaView,
MateriaDetail, MateriaCreate,
MateriaUpdate, MateriaDelete
)


app_name = 'materia'

router = routers.DefaultRouter()
router.register('api', MateriaViewSet)


urlpatterns = [
	path ('', include(router.urls)),
	path ('listado/', MateriaView.as_view(), name='listado'),
	path ('detalle/<int:pk>/', MateriaDetail.as_view(), name='detalle'),
	path ('crear/', MateriaCreate.as_view(), name='crear'),
	path ('modificar/<int:pk>/', MateriaUpdate.as_view(), name='modificar'),
	path ('borrar/<int:id>/', MateriaDelete.as_view(), name='borrar'),
]
