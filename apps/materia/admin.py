# -*- coding: utf-8 -*-

from django.contrib import admin
from .models import Materia

@admin.register(Materia)
class MateriaAdmin(admin.ModelAdmin):

	list_display = [
		'id', 'nombre','año',
		'fecha_hora', 'aula', 'profesores'
	]

	list_filter = ['nombre', 'año', 'aula']

	search_fields = ['nombre__icontains', 'año']
