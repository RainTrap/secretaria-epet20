# -*- coding: utf-8 -*-


from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic import ListView
from django.views.generic.edit import CreateView,UpdateView, DeleteView
from .models import Materia
from .serializers import MateriaSerializer


class MateriaViewSet(viewsets.ModelViewSet):

	queryset = Materia.objects.all()
	serializer_class = MateriaSerializer
	permission_classes = [DjangoModelPermissions] 

class MateriaView(LoginRequiredMixin, ListView):
	model = Materia
	template_name = 'materia/listado.html'
	context_object_name = 'materias'

class MateriaDetail(LoginRequiredMixin, DetailView):
	model = Materia
	template_name = 'materia/detalle.html'
	context_object_name = 'materia'

class MateriaCreate(LoginRequiredMixin, CreateView):
	model = Materia
	fields= ['nombre', 'año', 'fecha_hora', 'aula', 'profesores']
	template_name = 'materia/crear.html'
	success_url = reverse_lazy('materia:listado')

class MateriaUpdate(LoginRequiredMixin, UpdateView):
	model = Materia
	fields= ['nombre', 'año', 'fecha_hora', 'aula', 'profesores']
	template_name = 'materia/modificar.html'
	success_url = reverse_lazy('materia:listado')

class MateriaDelete(LoginRequiredMixin, DeleteView):
    template_name = 'materia/borrar.html'
    success_url = reverse_lazy('materia:listado')
    
    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Materia, id=id_)
