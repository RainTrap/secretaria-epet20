# -*- coding: utf-8 -*-

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from apps.materia.models import Materia

class RegistroMesaExamen(models.Model):
	nombre = models.CharField(max_length=40, help_text='Ingrese el nombre del alumno')
	apellido = models.CharField(max_length=40, help_text='Ingrese el apellido del alumno')
	dni = models.PositiveIntegerField(
		help_text='Ingrese el dni del alumno',
		validators=[
			MaxValueValidator(999999999),
			MinValueValidator(1000000)
		]
	)
	materia = models.ForeignKey(Materia, on_delete=models.CASCADE, help_text='Ingrese un materia a rendir') #Hay que hacer un ForeignKey (esperar a nahuel que suba sus cambios)
	correo_electronico = models.CharField(max_length=100, help_text='Ingrese un correo electronico para poder comunicarnos con el alumno')

	class Meta:
		verbose_name='registro para mesa de examen'
		verbose_name_plural='registros para mesas de examenes'

	def __str__(self):
		return '%s %s %s' % (self.nombre, self.apellido, self.materia)

