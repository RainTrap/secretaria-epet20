# -*- coding: utf-8 -*-
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions
from django.contrib.auth.mixins import LoginRequiredMixin
from .serializers import RegistroMesaExamenSerializer
from .models import RegistroMesaExamen
from django.shortcuts import get_object_or_404


class RegistroMesaExamenViewSet(viewsets.ModelViewSet):
	queryset = RegistroMesaExamen.objects.all()
	serializer_class = RegistroMesaExamenSerializer
	permission_classes = [DjangoModelPermissions]

class AlumnoMesaExamenView(LoginRequiredMixin, ListView):
	model = RegistroMesaExamen
	template_name = 'alumnos_mesas_examen/listado_alumnos.html'
	context_object_name = 'alumnos_registrados'

class AlumnoMesaExamenCreate(CreateView):
	model = RegistroMesaExamen
	fields= ['nombre', 'apellido', 'dni', 'materia', 'correo_electronico']
	template_name = 'alumnos_mesas_examen/registrar_alumno.html'
	success_url = reverse_lazy('alumnos_mesas:listado_alumnos')

class AlumnoMesaExamenDetail(LoginRequiredMixin, DetailView):
	model = RegistroMesaExamen
	template_name = 'alumnos_mesas_examen/detalle_alumno.html'
	context_object_name = 'alumno_registrado'

class AlumnoMesaExamenDelete(LoginRequiredMixin, DeleteView):
	model = RegistroMesaExamen
	template_name = 'alumnos_mesas_examen/borrar_alumno.html'
	success_url = reverse_lazy('alumnos_mesas:listado_alumnos')
	context_object_name = 'alumno_registrado'

class AlumnoMesaExamenUpdate(LoginRequiredMixin, UpdateView):
	model = RegistroMesaExamen
	fields = ['nombre', 'apellido', 'dni', 'materia', 'correo_electronico']
	template_name = 'alumnos_mesas_examen/modificar_alumno.html'
	success_url = reverse_lazy('alumnos_mesas:listado_alumnos')
	context_object_name = 'alumno_registrado'
