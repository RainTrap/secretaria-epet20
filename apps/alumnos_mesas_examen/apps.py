from django.apps import AppConfig


class AlumnosMesasExamenConfig(AppConfig):
    name = 'alumnos_mesas_examen'
