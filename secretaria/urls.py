# -*- coding: utf-8 -*-

from django.contrib import admin
from django.urls import path, include
from apps.core.views import GeneralView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('materia/', include('apps.materia.urls')),
    path('registro_mesa_examen/', include('apps.alumnos_mesas_examen.urls')),
    path('', GeneralView.as_view(), name='general'),
    path('accounts/', include('django.contrib.auth.urls')),
]
